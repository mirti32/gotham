import Vue from 'vue'
//import App from './PrototypeNicolas.vue'
import App from './TMS.vue'


Vue.config.productionTip = false

new Vue({
  render: h => h(App),
}).$mount('#app')
